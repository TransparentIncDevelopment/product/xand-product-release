[[_TOC_]]

# Xand Product Release

This repository will contain the versions of artifacts which should be made available publicly. Merge Requests that modify the manifests with software versions have passed the quality gates for releasing the artifacts at the time of Merge Request approval.

Reasons for having this repo:

* Only production-quality artifacts should be made available to external partners.
* There should be conscious intention around what's made available to our partners.
* Release decisions should be tracked and auditable.

# Making a Merge Request

Since there is a quality bar to what we can release, an MR should contain all relevant information to show that it qualifies for release. Initially this might be fairly manual and may require leg work. **You don't have to do this alone.** There should be a merge request template that will ask for any of the relevant information regarding the quality or security tests that need to pass. **Please @mention developers in the MR that should be able to help and create associated Azure DevOps tracking items.**

These are the quality bars that we will have initially (This list is likely not to be kept up to date, see MR template for most up to date details):
* Passing Acceptance Tests in `xand-k8s` that contain the combination of the versions updated in the MR.
* Test Scenarios passed in an SGD.
* Links to tags for relevant repositories.
* Cargo Audit re-run recently for related repositories
* Any ignored cargo audit rules for those repositories

# Who can approve a Merge Request

Approvals will be made by **Qualified Stakeholders** may change, and should only be made by executive leadership. The **Qualified Stakeholders** group are expected to meet the criteria below.

* **Qualified Stakeholders** would be determined by executive leadership.
* **Qualified Stakeholders** should be small enough that decisions can be made quickly.
* **Qualified Stakeholders** should be large enough that releases are not held up due to vacations.
* A suggestion for this group is to have 2 representatives from Product & Engineering.

# Automation of Release

Once the MR that modifies either the `software_versions.env` or `devops_versions.env` is merged to master. The corresponding artifacts will be released publicly via CI if not already published.

## release_artifacts.py script

The script will take in several arguments that it can either pull from an environment variable or passed via an option flag. Please see the help section anything that ends with [ARTIFACTORY_USERNAME] means it default to pull from an environment variable (useful when running in CI). Passing 3 mandatory items. The manifest file for software version, the manifest file for devops version, and a non-existent directory as a working directroy.

The CI pipeline will always run the script in dry-run mode to make sure it can accomplish everything except the publish part without issue.

```shell
$ ./release_artifacts.py -h
usage: release_artifacts.py [-h] [--artifactory-username ARTIFACTORY_USERNAME]
                            [--artifactory-password ARTIFACTORY_PASSWORD]
                            [--base64-gcloud-dev-service-key BASE64_GCLOUD_DEV_SERVICE_KEY]
                            [--ci-job-url CI_JOB_URL] [--ci-branch CI_BRANCH] [--ci-pipeline-id CI_PIPELINE_ID]
                            [--dry-run]
                            SOFTWARE_VERSION_CONFIG DEVOPS_VERSION_CONFIG OUTPUT_DIR

Release artifacts for public use.

positional arguments:
  SOFTWARE_VERSION_CONFIG
                        The path to the software version config that has the versions of the software to release.
  DEVOPS_VERSION_CONFIG
                        The path to the devops version to release.
  OUTPUT_DIR            The output folder where the deployment artifacts will land.

optional arguments:
  -h, --help            show this help message and exit
  --artifactory-username ARTIFACTORY_USERNAME
                        The Artifactory username of the user that has permission to release artifacts.
                        [ARTIFACTORY_USER]
  --artifactory-password ARTIFACTORY_PASSWORD
                        The Artifactory password of the user that has permission to release artifacts.
                        [ARTIFACTORY_PASS]
  --base64-gcloud-dev-service-key BASE64_GCLOUD_DEV_SERVICE_KEY
                        The base64 encoded google cloud dev service account's key for pulling pre-release image from
                        gcr. [GCLOUD_DEV_SERVICE_KEY]
  --ci-job-url CI_JOB_URL
                        The job url that produced all these artifacts.[CI_JOB_URL]
  --ci-branch CI_BRANCH
                        The git branch that produced all these artifacts.[CI_BRANCH]
  --ci-pipeline-id CI_PIPELINE_ID
                        The pipeline id that produced all these artifacts.[CI_PIPELINE_ID]
  --dry-run             Whether to run this in dry-run mode which won't release artifacts and just print messages as
                        an example.
```

When running it in dry run mode you'll see messages like the following for zip vs docker:

Zip:
```bash
Downloading url 'https://transparentinc.jfrog.io/artifactory/artifacts-internal/cert-manager-partners-k8s/cert-managerutput/cert-manager-partners-k8s.1.0.2.zip'
--- Here is where we would upload 'output/cert-manager-partners-k8s.1.0.2.zip' to 'https://transparentinc.jfrog.io/art-partners-k8s/cert-manager-partners-k8s.1.0.2.zip' ---
```

Docker:
```bash
Pulling docker image 'gcr.io/xand-dev/validator:1.0.7'!
--- Here is where would push the image 'transparentinc-docker-external.jfrog.io/validator:1.0.7'
```
