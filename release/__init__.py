from release.artifact_transformation import ArtifactTransformationInterface

from release.ansible_transformation import AnsibleTransformation
from release.artifactory_release import ArtifactoryRelease
from release.docker_release import DockerRelease
from typing import Dict, Optional


def release_artifacts(artifactory_releaser: ArtifactoryRelease, docker_releaser: DockerRelease, versions: Dict, output_dir: str) -> None:
    ansible_transformation = AnsibleTransformation(versions)

    for artifact_name, artifact_object in versions.items():
        print(f"Starting release for '{artifact_name}'!")
        transformation = ansible_transformation if artifact_name == "ansible" else None

        if 'zips' in artifact_object:
            for zip_object in artifact_object['zips']:
                _release_artifactory_artifacts_if_possible(
                    artifactory_releaser,
                    zip_object,
                    output_dir,
                    transformation
                )

        if 'docker_images' in artifact_object:
            for image in artifact_object['docker_images']:
                _release_docker_artifacts_if_possible(
                    docker_releaser,
                    image
                )

        print(f"Finished releasing '{artifact_name}'!\n\n")


def _release_artifactory_artifacts_if_possible(
        artifactory_releaser: ArtifactoryRelease,
        artifact_object: Dict,
        output_dir: str,
        transformation: Optional[ArtifactTransformationInterface]) -> None:

    zip_name = artifact_object['name']
    zip_repo = artifact_object['repo']
    zip_version = artifact_object['version']

    # If neither are specified then there's nothing to do.
    if not zip_name and not zip_version and not zip_repo:
        return

    if not zip_name:
        raise Exception(f"The name of the zip was not specified. Fields: repo is '{zip_repo}' and version is '{zip_version}'.")

    if not zip_repo:
        raise Exception(f"The repo of the zip was not specified. Fields: name is '{zip_name}' and version is '{zip_version}'.")

    if not zip_version:
        raise Exception(f"The version of the zip was not specified. Fields: name is '{zip_name}' and repo is '{zip_repo}'.")

    artifactory_releaser.release(working_dir=output_dir, repo=zip_repo, package_name=zip_name, version=zip_version, transformation=transformation)


def _release_docker_artifacts_if_possible(docker_releaser: DockerRelease, artifact_object: Dict) -> None:
    docker_image = artifact_object['image_name']
    docker_version = artifact_object['image_version']

    # If neither are specified then there's nothing to do.n
    if not docker_image and not docker_version:
        return

    if not docker_image:
        raise Exception(f"The image_name was not specified. Fields: version is '{docker_version}'.")

    if not docker_version:
        raise Exception(f"The image_version was not specified. Fields: image_name is '{docker_image}'.")

    docker_releaser.release(docker_image, docker_version)
