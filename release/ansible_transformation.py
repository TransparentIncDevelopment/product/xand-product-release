from . import ArtifactTransformationInterface
import re
import os
from os.path import basename, exists, join
from shutil import rmtree
from typing import Dict
import zipfile

def _get_docker_image_version(all_versions: Dict, component_name: str, image_name: str) -> str:
    """A static function that retrieves the docker image version based on the name of the component
       and the name of the image.
    """
    images = all_versions[component_name]['docker_images']
    image_object = next(filter(lambda image: image['image_name'] == image_name, images), None)

    if not image_object:
        raise Exception(f"Could not find an image with name '{image_name}' for the component '{component_name}'")

    return image_object['image_version']

def _get_zip_version(all_versions: Dict, component_name: str, zip_repo: str, zip_name: str) -> str:
    """A static function that retrieves the zip version based on the name of the component
       name of the zip and the name of the repo the zip belongs to.
    """
    zips = all_versions[component_name]['zips']
    zip_object = next(filter(lambda zip: zip['repo'] == zip_repo and zip['name'] == zip_name, zips), None)

    if not zip_object:
        raise Exception(f"Could not find a zip with name '{zip_name}' and repo '{zip_repo}' for the component '{component_name}'")

    return zip_object['version']

# This dictionary is the list of versions in ansible that would need to be replaced and how to retrieve them given the python object.
RETRIEVE_VERSIONS_FUNCS = {
    "xand_node_version": lambda versions: _get_docker_image_version(versions, 'validator', 'validator'),
    "xand_node_k8s_version": lambda versions: _get_zip_version(versions, 'validator', 'consensus-xand-node', 'consensus-xand-node'),
    "prometheus_pipe_srv_version": lambda versions: _get_docker_image_version(versions, 'prometheus_pipesrv', 'prometheus-pipesrv'),
    "member_api_version": lambda versions: _get_docker_image_version(versions, 'member_api', 'member-api'),
    "trust_k8s_version": lambda versions: _get_zip_version(versions, 'trust', 'trust-k8s', 'trust-k8s'),
    "trust_image_version": lambda versions: _get_docker_image_version(versions, 'trust', 'trust'),
    "toolchain_version": lambda versions: _get_docker_image_version(versions, 'xand_devops', 'xand-devops-toolchain'),
    "xand_devops_versions": lambda versions: _get_zip_version(versions, 'xand_devops', 'xand-devops', 'xand-devops'),
    "cert_manager_version": lambda versions: _get_zip_version(versions, 'cert_manager', 'cert-manager-partners-k8s', 'cert-manager-partners-k8s'),
    "grafana_version": lambda versions: _get_zip_version(versions, 'grafana', 'grafana-k8s', 'grafana-k8s'),
    "loggly_version": lambda versions: _get_zip_version(versions, 'fluentd_loggly', 'fluentd-loggly-k8s', 'fluentd-loggly-k8s'),
    "nginx_version": lambda versions: _get_zip_version(versions, 'nginx_ingress', 'nginx-controller-k8s', 'nginx-controller-k8s'),
    "prometheus_version": lambda versions: _get_zip_version(versions, 'prometheus', 'prometheus-k8s', 'prometheus-k8s')
}

class AnsibleTransformation(ArtifactTransformationInterface):
    def __init__(self,
                 all_versions: Dict):

        self.all_versions = all_versions


    def transform(self, working_dir: str, artifact_identifier: str) -> str:
        """Performs a transformation on the zip that contains ansible definitions and outputs them to a place in the working_dir.

        :param working_dir: The working directory for the transformation to publish artifacts from.
        :type working_dir: str
        :param artifact_identifier: The path to the ansible zip that will be transformed.
        :type artifact_identifier: str

        :returns: An identifier after the transformation has completed to use for publishing.
        :rtype: str
        """
        filename = basename(artifact_identifier)
        folder_name = filename[: filename.find(".")]
        ansible_transform_dir = join(working_dir, "ansible_transform")
        extract_dir = join(ansible_transform_dir, folder_name)

        if not exists(ansible_transform_dir):
            os.mkdir(ansible_transform_dir)

        if not exists(extract_dir):
            os.mkdir(extract_dir)

        with zipfile.ZipFile(artifact_identifier, 'r') as zip_ref:
            zip_ref.extractall(extract_dir)

        self._replace_versions(extract_dir)
        self._rewrite_tpfs_values(extract_dir)

        output_zip_path = join(ansible_transform_dir, basename(artifact_identifier))
        self._zip_output(extract_dir, output_zip_path)

        return output_zip_path

    def _replace_versions(self, extract_dir: str) -> None:
        constants_file_path = join(extract_dir, "vars", "constants.yml")

        with open(constants_file_path, "r") as f:
            constants_content = f.read()

        # Loop through all of the functions that will retrieve the version for the corresponding key name in the constants.yaml
        # It will then perform a replacement of the key name like `xand_node_version: "1.4.5"` to `xand_node_version: "6.2.0"`
        # and preserve that in the same variable so the next replacement will occur on the updated contents.
        for key, func in RETRIEVE_VERSIONS_FUNCS.items():
            version = func(self.all_versions)
            constants_content = re.sub(f'{key}:[\\s]*"[^"]*"', f'{key}: "{version}"', constants_content)

        print(f"Rewrote the contents of '{constants_file_path}'.")

        with open(constants_file_path, "w") as f:
            f.write(constants_content)

    def _rewrite_tpfs_values(self, extract_dir: str) -> None:
        tpfs_supplied_file_path = join(extract_dir, "vars", "tpfs_supplied_values.yml")

        with open(tpfs_supplied_file_path, "r") as f:
            tpfs_supplied_content = f.read()

        # Removing 5 line section in yaml file. The following section:
        # # For non-public feeds and when you're deploying to non-google-cloud provider, replace gke.service_account_src with a path.
        # # leave space ' ' in front of the lookup: https://stackoverflow.com/a/57267055/1807040
        # # Example for non-public-feed:
        # # google_cloud_service_account: " {{ lookup('file', '~/my_gcr_service_account') }}"
        # google_cloud_service_account: " {{ lookup('file', gke.service_account_src) }}"
        tpfs_supplied_content = re.sub("\n# For non-public.*\n.*\n.*\n.*\n.*\n", "" , tpfs_supplied_content)
        tpfs_supplied_content = re.sub("public_feed:[\\s]*no", "public_feed: yes" , tpfs_supplied_content)

        print(f"Rewrote the contents of '{tpfs_supplied_file_path}'.")

        with open(tpfs_supplied_file_path, "w") as f:
            f.write(tpfs_supplied_content)

    def _zip_output(self, files_dir: str, zip_file_path: str):
        zipf = zipfile.ZipFile(zip_file_path, mode='w')
        output_dir_length = len(files_dir)

        for root, _unused_, files in os.walk(files_dir):
            for file in files:
                file_path = os.path.join(root, file)
                # Strip out the output dir path from the file path
                zipf.write(file_path, file_path[output_dir_length:])
        zipf.close()
