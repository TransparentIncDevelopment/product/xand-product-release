from . import ArtifactTransformationInterface
from os.path import join
import requests
from typing import Optional
from urllib.parse import urljoin

class ArtifactoryRelease:

    def __init__(self,
                 username: str,
                 password: str,
                 pre_release_url: str,
                 release_url: str,
                 dry_run: bool,
                 ci_job_url: str = None,
                 ci_branch: str = None,
                 ci_pipeline_id: str = None):

        self.auth = (username, password)
        self.pre_release_url = pre_release_url
        self.release_url = release_url
        self.dry_run = dry_run
        self.ci_job_url = ci_job_url
        self.ci_branch = ci_branch
        self.ci_pipeline_id = ci_pipeline_id

    def release(self, working_dir: str, repo: str, package_name: str, version: str, transformation: Optional[ArtifactTransformationInterface]) -> None:
        download_base_url = urljoin(self.pre_release_url, repo +"/")
        filename = f"{package_name}.{version}.zip"
        file_path = self._download_file(working_dir, download_base_url, filename)

        if transformation:
            file_path = transformation.transform(working_dir, file_path)

        upload_base_url = urljoin(self.release_url, repo +"/")
        self._upload_file(file_path, upload_base_url, filename)

    def _download_file(self, working_dir: str, base_url: str, filename: str) -> str:
        file_path = join(working_dir, filename)
        url = urljoin(base_url, filename)

        print(f"Downloading url '{url}' to file path '{file_path}'")
        with requests.get(url, auth=self.auth, stream=True) as r:
            r.raise_for_status()
            with open(file_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    # If you have chunk encoded response uncomment if
                    # and set chunk_size parameter to None.
                    #if chunk:
                    f.write(chunk)

        return file_path


    def _upload_file(self, file_path: str, base_url: str, filename: str) -> None:
        properties = []

        url = urljoin(base_url, filename)

        if self.ci_job_url:
            properties.append(f"job_url={self.ci_job_url}")

        if self.ci_branch:
            properties.append(f"branch={self.ci_branch}")

        if self.ci_pipeline_id:
            properties.append(f"pipeline_id={self.ci_pipeline_id}")

        if len(properties) > 0:
            url += ";" + ";".join(properties)

        if self.dry_run:
            print(f"--- Here is where we would upload '{file_path}' to '{url}' ---")
        else:
            print(f"Uploading file path '{file_path}' to '{url}'")
            with open(file_path, 'rb') as f:
                r = requests.put(url, auth=self.auth, data=f)
                # Used to raise an error, but now merely prints an error and moves on.
                # This is a quick pragmatic fix to ADO 7117.
                if not r.ok:
                    print(f"Error encountered while uploading file to '{url}': '{r.reason}'")
