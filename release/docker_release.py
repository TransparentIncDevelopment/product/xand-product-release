from docker import DockerClient  # type: ignore
from registry import DockerRegistry
from urllib.parse import urljoin

class DockerRelease:
    def __init__(self,
                 username: str,
                 password: str,
                 pre_release_registry: DockerRegistry,
                 release_registry: DockerRegistry,
                 client: DockerClient,
                 dry_run: bool):

        self.username = username
        self.password = password
        self.pre_release_registry = pre_release_registry
        self.release_registry = release_registry
        self.client = client
        self.dry_run = dry_run

    def release(self, image_name: str, version: str) -> None:
        pre_release_repository = urljoin(self.pre_release_registry.registry, image_name)
        release_repository = urljoin(self.release_registry.registry, image_name)

        print(f"Pulling docker image '{pre_release_repository}:{version}'!")
        image = self.client.images.pull(repository=pre_release_repository, tag=version)

        # Tag the image from the pre_release_registroy to the release registry.
        image.tag(release_repository, tag=version)

        if self.dry_run:
            print(f"--- Here is where would push the image '{release_repository}:{version}' ---")
        else:
            print(f"Pushing docker image '{release_repository}:{version}'!")
            self.client.images.push(
                repository=release_repository,
                tag=version,
                auth_config={
                    "username": self.username,
                    "password": self.password
                }
            )
