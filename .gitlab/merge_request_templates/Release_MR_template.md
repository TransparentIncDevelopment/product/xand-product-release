##### Please remove the bolded/italicized text when you complete a section below.

# MR Summary

**_Information relevant to explaining your Merge Request belongs here._**

Just a reminder from the README.md that you don't have to fill this out alone.  
**You don't have to do this alone.**

**Please @mention developers in the MR that should be able to help and create associated Azure DevOps tracking items.**

**_Thanks for providing the MR summary here._**

## Relevant Artifacts
**_Artifacts relevant to your Merge Request belong here._**

**_The specific acceptance test and .env URLs needs to be replaced below_**

.Env file:  
https://gitlab.com/TransparentIncDevelopment/product/devops/xand-k8s/-/blob/COMMIT_HASH/.env

Corresponding acceptance tests:  
https://gitlab.com/TransparentIncDevelopment/product/devops/xand-k8s/-/jobs/

**_Thanks for providing an acceptance tests job URL and .env file_**

**_The specific SGD excel sheet URL needs to be replaced below with passing acceptance tests_**

https://transparentfinancialsystemsn.sharepoint.com/:f:/s/TransparentSystems/EoYc3nMutnpMlVhIupGrVaUB9fT1kui5LWSHE78VO493nQ?e=bPjfMH

**_Thanks for providing the specific SGD excel sheet URL and .env file_**

----------------------------------------------------------

### Validator
Tagging for Validator:
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for Validator:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### Member
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/member_api/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for member:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/member_api/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### Trust
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/xand-trust/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for Trust:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/xand-trust/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### Council Voting CLI
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/council_voting_cli/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for Council Voting CLI:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/council_voting_cli/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------
### JWT CLI
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/jwtcli/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for JWT CLI:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/jwtcli/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### XKeyGen
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/apps/xkeygen/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for XKeyGen:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/apps/xkeygen/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### xand_banks
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/libs/xand_banks/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for xand_banks:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/libs/xand_banks/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### tpfs_krypt
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/libs/tpfs_krypt/-/tags

**_Thanks for updating the tag URL_**

Cargo Audit for tpfs_krypt:
**_Please see the [documentation](path) on Confluence if you need help finding and re-running the job_**

https://gitlab.com/TransparentIncDevelopment/product/libs/tpfs_krypt/-/jobs

**_Thanks for providing a freshly-run job URL_**
<!-- TODO: Create generic confluence documentation for finding job #-->

Cargo Ignored RUSTSEC IDs:

**Please provide the RUSTSEC-YYYY-#### IDS located in the audit.toml file**

- RUSTSEC-YYYY-####

**_Thanks for providing those RUSTSEC IDs_**

----------------------------------------------------------

### Xand Devops
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/xand-devops/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

### Ansible
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/ansible/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

### Cert Manager
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/cert-manager/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

### Nginx Ingress
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/nginx-controller-k8s/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

### Monitoring
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/monitoring/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

### Loggly
**_The specific tagging URL needs to be replaced below_**

https://gitlab.com/TransparentIncDevelopment/product/devops/loggly/-/tags

**_Thanks for updating the tag URL_**

----------------------------------------------------------

<!-- Pin: Add applicable labels to meet requirements. May not be valuable for our use and can be removed from template if so.
/label ~MR ~release ~YOUR-OTHER-LABELS-HERE
-->

<!-- Tag appropriate approvers; @Person or @Group will work.
Eventually an @Stakeholder should be set as default, but none exists yet.

@YOUR-TAGGED-APPROVERS-HERE
-->
