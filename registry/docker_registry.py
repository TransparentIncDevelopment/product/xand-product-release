from docker import DockerClient  # type: ignore

class DockerRegistry:
    def __init__(
            self,
            registry: str,
            username: str,
            password: str):

        self.registry = registry
        self.username = username
        self.password = password

    def client_login(
            self,
            client: DockerClient) -> None:

        client.login(
            username=self.username,
            password=self.password,
            registry=self.registry
        )
