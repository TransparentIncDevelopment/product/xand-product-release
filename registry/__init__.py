import docker  # type: ignore
from docker import DockerClient  # type: ignore
from registry.docker_registry import DockerRegistry

def initialize_docker_client(pre_release_registry: DockerRegistry, release_registry: DockerRegistry) -> DockerClient:
    client = docker.from_env()

    # Ping the docker client and make sure we can talk to the docker engine.
    client.ping()

    pre_release_registry.client_login(client)
    release_registry.client_login(client)

    return client
